// We do not need to show time from 00:00:00 to 08:00:00,
// because no one will work that time
export const DAY_STARTS_FROM = '08:00:00';
// Where the day ends
// If day ends at midnight, put 24:00:00, not 00:00:00 here.
export const DAY_ENDS_AT = '21:00:00';