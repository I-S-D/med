export default function () {
  return {
    ru: {
      cabinet: 'Кабинет $1',
      infoLink: 'Информация'
    }
  }['ru']
}