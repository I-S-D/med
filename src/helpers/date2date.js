/*
 * Datatime
 * Used to convert text-based time
 * to Date
*/

// Convert from time like 12:13:14 and 
// date like 19.02.2021 to JS Date
export default function txt2date(date, time) {
  // Convert from 19.02.2021 to 2021-02-19
  date = date.split('.');
  date.reverse();
  date = date.join('-');
  return new Date(date + 'T' + time + 'Z');
}