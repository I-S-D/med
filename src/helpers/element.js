/**
 * Element
 * Tiny wrapper for DOM API
 * to create elements like
 * _('span').class('demo', 'awesome')._
 */

export default class _ {
  constructor (tag) {
    this._ = document.createElement(tag);
  }

  class(...classNames) {
    this._.classList.add(...classNames);
    return this;
  }

  id(id) {
    this._.setAttribute('id', id);
    return this;
  }

  has(...children) {
    children.forEach(i => this._.appendChild(i instanceof _ ? i._ : i));
    return this;
  }

  find(selector) {
    return this._.querySelector(selector);
  }

  text(text) {
    this._.textContent = text;
    return this;
  }

  attr(name, value) {
    this._.setAttribute(name, value);
    return this;
  }
};