import getCalendar from "../api/calendar.js";
import _ from "../helpers/element.js";
import txt2date from "../helpers/date2date.js";
import { DAY_STARTS_FROM, DAY_ENDS_AT } from "../config/config.js";
import getStrings from '../config/i18n.js'

const strings = getStrings();

function secsFromDayStart(date) {
  return date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
}

function removeSeconds(str) {
  return str.slice(0, str.lastIndexOf(":"));
}

class Calendar extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });

    let style = new _('link').attr('rel', 'stylesheet').attr('href', '../styles/calendar.css');
    this.shadowRoot.appendChild(style._);

    let layout = new _('div').class('-main-layout');
    this.shadowRoot.appendChild(layout._);
    this.layout = layout;
    this.counter = 0;
  }

  putData(data, span) {
    this.length = span;
    this.setupTimeline(Object.keys(data).length);
    for (let i of Object.keys(data)) {
      this.putEvents(i, data[i]);
    }
  }

  setupTimeline(blocks) {
    if (blocks < 10) {
      // Standard form
      // -----------------
      //  9:00 |  Pupkin
      // 10:00 |  Vasukov
      // 11:00 |  Vasukov
      let $timeline = this.layout.find('.-timeline');
      if (!$timeline) {
        $timeline = new _('div').class('-timeline')._;
        let startHour = (DAY_STARTS_FROM.split(':')[0])|0;
        let endHour = (DAY_ENDS_AT.split(':')[0])|0;
        for (let i = startHour; i <= endHour; ++i)
          $timeline.appendChild(new _('span').text((i < 10 ? '0' + i : i) + ':00')._);
        this.layout._.insertBefore($timeline, this.layout._.firstChild);
      }
      this.hasTimeStamps = true;
    } else {
      // Grid-like view
      // We do not need timestamps
      this.hasTimeStamps = false;
    }
  }

  putEvents(date, events) {
    let $day = new _('div').class('-day');
    $day.has(new _('span').class('-date').text(date))
    for (let i of events) {

      // Create event element
      let $event = new _('div').class('-event').has(
        new _('span').class('-name').text(i.name)
      )

      let $spacer = new _('span').class('-spacer');
      $event.has($spacer);

      if (this.hasTimeStamps) {
        let  start = txt2date(date, i.start),
              end = txt2date(date, i.end),
              shift = txt2date(date, DAY_STARTS_FROM);
        let startSeconds = secsFromDayStart(start);
        let endSeconds   = secsFromDayStart(end);
        let shiftSeconds = secsFromDayStart(shift);
        let diff = endSeconds - startSeconds;

        // Add time data if needed
        $event.attr('style', `--start: ${startSeconds - shiftSeconds}; --diff: ${diff}; --anim-offset: ${this.counter}`);
        ++this.counter;
        setTimeout(function () {
          function lines() {
            let height = getComputedStyle($spacer._).height.replace('px', '')|0;
            let fontSize = parseFloat(getComputedStyle($event._).fontSize);
            return height/fontSize;
          }
          let $time = new _('span').class('-time').text(removeSeconds(i.start) + '-' + removeSeconds(i.end));
          if (lines() < 1)
            $time.class('--invisible');
          $event._.insertBefore($time._, $spacer._)
          let $cabinet = new _('span').class('-cabinet').text(strings.cabinet.replace(/\$1/g, i.cabinet));
          if (lines() < 1)
            $cabinet.class('--invisible');
          $event._.insertBefore($cabinet._, $spacer._)

          let $infoLink = new _('a').attr('href', '/user/' + i.userid).text(strings.infoLink).class('-info-link', '--invisible');
          $event.has($infoLink)

        }, 10)
      }
      $day.has($event);
    }
    this.layout.has($day);
  }
};

customElements.define('m-calendar', Calendar);