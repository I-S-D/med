export default function getUserData(userId) {
  return {
    13: {
      name: 'Василий Пупкин',
      props: {
        'Возраст': 49,
        'Вес': 80,
        'Рост': 178
      }
    },
    14: {
      name: 'Пётр Петрович',
      props: {
        'Возраст': 100,
        'Вес': 75,
        'Рост': 173
      }
    }
  }[userId];
}