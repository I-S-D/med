/**
 * This function gets calendar events
 * Now, while there is no API, it is just a stub
 */
export default function getCalendar() {
  // Note what it is in UTC+0, not local time.
  return {
    '02.03.2042': [
      { start: '12:00:00', end: '13:00:00', name: 'Василий Пупкин', cabinet: 'Z42', userid: 13 },
      { start: '14:30:00', end: '15:00:00', name: 'Пётр Петрович',  cabinet: 'A3',  userid: 14 },

      { start: '18:00:00', end: '18:05:00', name: 'Пётр Петрович',  cabinet: 'A3',  userid: 14 },
      { start: '18:05:00', end: '18:10:00', name: 'Пётр Петрович',  cabinet: 'A3',  userid: 14 },
      { start: '18:10:00', end: '18:15:00', name: 'Пётр Петрович',  cabinet: 'A3',  userid: 14 }
    ],
    '03.03.2042': [
      { start: '15:00:00', end: '15:30:00', name: 'Василий Пупкин', cabinet: 'Z42', userid: 13 },
      { start: '16:00:00', end: '18:00:00', name: 'Василий Пупкин', cabinet: 'Z42', userid: 13 }
    ]
  }
}